package com.example.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class homescreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homescreen);
        TextView name=findViewById(R.id.name);
        Button exit=findViewById(R.id.exit_button);

        String username =getIntent().getStringExtra("name");

        name.setText(username);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(homescreen.this,MainActivity.class);
                startActivity(i);
                finish();


            }
        });



    }
}