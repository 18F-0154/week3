package com.example.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Signup extends AppCompatActivity {

Button signup;
EditText email,firstname,lastname,pass,repeat_pass,number;

boolean checking=false,pass_valid=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        signup=findViewById(R.id.signed);
        email=findViewById(R.id.signup_email);
        firstname=findViewById(R.id.firstname);
        lastname=findViewById(R.id.lastname);
        number=findViewById(R.id.number);
        pass=findViewById(R.id.signup_password);
        repeat_pass=findViewById(R.id.repeat_password);


        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checking= checkers();

                if(checking){
                   Intent i =getIntent();
                   String get_email=i.getStringExtra("email");
                   String passw=i.getStringExtra("password");
                   Intent j=new Intent(Signup.this,MainActivity.class);
                   startActivity(j);

                }



            }
        });


    }
    private boolean checkers(){
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if(firstname.length()==0){
            firstname.setError("First name required");
            return false;
        }
        if(lastname.length()==0){
            lastname.setError("last name required");
            return false;
        }
        if(email.length()==0){
            email.setError("Email required");
            return false;
        }else if(email.getText().toString().matches(emailPattern)){
            String donothing;
        }else{
            email.setError("invalid email");
                    return false;
        }

        if(pass.length()==0){
            pass.setError("password required");
            return false;
        }else if(pass.length()>8){
            pass.setError("Minimum 8 characters");
            return false;
        }
        if(repeat_pass.length()==0 && pass.length()>0){
            repeat_pass.setError("password confirmation required");
                    return false;
        }
        if(pass.getText().toString().equals(repeat_pass)){
            repeat_pass.setError("Password not matches");
            return false;
        }
        if(number.length()==0){
            number.setError("phone number required");
            return false;
        }
        


        return true;
    }

}